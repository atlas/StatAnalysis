# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building xRooFit for StatAnalysis releases
#

# The name of the package:
atlas_subdir( xRooFit )

set( STATANA_XROOFIT_REPOSITORY "${CERN_GITLAB_PREFIX}/will/xroofit.git" CACHE STRING "Repository of xroofit" )

set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/xRooFitBuild )

if(STATANA_VERBOSE)
    set(_logging OFF)
else()
    set(_logging ON)
endif()

# Build lwtnn for the build area:
ExternalProject_Add( xRooFit
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        GIT_REPOSITORY ${STATANA_XROOFIT_REPOSITORY}
        GIT_TAG ${STATANA_XROOFIT_VERSION}
        BUILD_ALWAYS ${TRACK_CHANGES}
        CMAKE_CACHE_ARGS
        -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}  # ensures will discover the release's version of ROOT
        LOG_DOWNLOAD ${_logging} LOG_CONFIGURE ${_logging} LOG_BUILD ${_logging} LOG_INSTALL ${_logging}
        LOG_OUTPUT_ON_FAILURE 1
        UPDATE_COMMAND "" # needed for next line to work
        UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning.
ExternalProject_Add_Step( xRooFit buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} <INSTALL_DIR>
        COMMENT "Installing xRooFit into the build area"
        DEPENDEES install
        )

if( ATLAS_BUILD_ROOT )
    add_dependencies ( xRooFit ROOT )
endif()


# Install xRooFit:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES cmake/FindxRooFit.cmake
        DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
