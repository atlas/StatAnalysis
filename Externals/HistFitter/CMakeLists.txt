# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building HistFitter for StatAnalysis releases
#

# The name of the package:
atlas_subdir( HistFitter )

if( STATANA_HISTFITTER_VERSION STREQUAL "" )
    message(STATUS "NOT BUILDING: HistFitter")
    return()
endif()

set( STATANA_HISTFITTER_REPOSITORY "https://github.com/histfitter/histfitter.git" CACHE STRING "Repository of HistFitter" )


set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HistFitterBuild )

if(STATANA_VERBOSE)
    set(_logging OFF)
else()
    set(_logging ON)
endif()

# Build lwtnn for the build area:
ExternalProject_Add( HistFitter
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        GIT_REPOSITORY ${STATANA_HISTFITTER_REPOSITORY}
        GIT_TAG ${STATANA_HISTFITTER_VERSION}
        BUILD_ALWAYS ${TRACK_CHANGES}
        CMAKE_CACHE_ARGS
        -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}  # ensures will discover the release's version of ROOT
        LOG_DOWNLOAD ${_logging} LOG_CONFIGURE ${_logging} LOG_BUILD ${_logging} LOG_INSTALL ${_logging}
        UPDATE_COMMAND "" # needed for next line to work
        UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning.
ExternalProject_Add_Step( HistFitter buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} <INSTALL_DIR>
        COMMENT "Installing HistFitter into the build area"
        DEPENDEES install
        )
add_dependencies( Package_HistFitter HistFitter )

if( ATLAS_BUILD_ROOT )
    add_dependencies ( HistFitter ROOT )
endif()


# Install HistFitter:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

